import java.util.Scanner;

public class HomeWorkJava {
    public static void main(String[] args) {
        //Задание №1
        for (int d = 5; d > 0; d--) {
            System.out.println(d);
        }
        //или
        int a = 6;
        while (a > 1) {
            a--;
            System.out.println(a);
        }
        //Задание №2
        String[][] array = new String[10][1];
        array[0][0] = "3";
        array[1][0] = "6";
        array[2][0] = "9";
        array[3][0] = "12";
        array[4][0] = "15";
        array[5][0] = "18";
        array[6][0] = "21";
        array[7][0] = "24";
        array[8][0] = "27";
        array[9][0] = "30";
        for (int i = 0; i <= 9; i++) {
            for (int j = 0; j < 1; j++) {
                System.out.println("3" + " * " + (i + 1) + " = " + array[i][j]);
            }
        }
        // Задание №3 с использованием Scanner
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите число:");
        int number = sc.nextInt();
        System.out.println("Спасибо! Вы ввели число " + number);
        int result = 0;
        for (int x = 1; x <= number; x++) {
            result += x;
        }
        System.out.print("Сумма введенного Вами числа равна: " + result);

    }
}


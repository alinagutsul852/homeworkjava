public class SumOfDigits {
    public static void main(String[] args) {
        System.out.println(sumOfDigitsFrom1ToN(8));
    }
    static int sumOfDigitsFrom1ToN(int n) {
        int result = 0;
        for (int x = 1; x <= n; x++) {
            result += x;
        }
        return result;
    }
}
